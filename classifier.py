import copy
import data
import random
import time
import numpy
import preprocessor

import pprint as pp

from monte_carlo import MonteCarlo

from helpers import showImage, readImage, convertDict, extendDict, exportToCsv, exportToJson

class Classifier:

    def __init__(self, matcher, settings, verbose=False):
        self.matcher = matcher
        self.settings = settings
        self.data = data.getFiles(self.settings['data_path']+'*.jpg')
        self.templates = data.getFiles(self.settings['templates_path']+'*.jpg')
        self.result = {}
        self.timer = 0
        self.verbose = verbose

    ##### MAIN #####

    # This method initializes classification
    def classify(self, image = False):
        self.timer = time.time()

        if self.settings['optimization']['method'] == 'MonteCarlo':
            monte_carlo = MonteCarlo(self.matcher, self.settings, image)
            self.result = monte_carlo.run()

        self.timer = time.time()-self.timer

        return self.result

    # Get class of a pattern based on the highest match
    def getClass(self, pattern_name):
        winner = self.settings['unknown_class']
        best_match = 0

        for result in self.result[pattern_name]:
            if result['match'] > best_match:
                winner = result['template_name']
                best_match = result['match']

        return winner

    # Calculate performance measures and print them
    def calcPerformance(self):
        pattern_categories = list(set([data.getCategory(data.getFilename(x)) for x in self.data]))
        pattern_classes = [data.getCategory(data.getFilename(x)) for x in self.templates]

        row = {i : 0 for i in pattern_classes}
        row[self.settings['unknown_class']] = 0

        confusion_matrix = {i : copy.deepcopy(row) for i in pattern_categories}

        for pattern_name, result in self.result.items():
            # The actual class of a pattern
            pattern_category = result['category']

            # How the pattern was classified
            pattern_class = result['class']

            # Increment counter
            confusion_matrix[pattern_category][pattern_class] += 1

        if self.verbose:
            print ''
            for category, row in confusion_matrix.items():
                print category, ':', row, '\n'

        # accuracy = number of patterns correctly classified as i / number of patterns in class i
        accuracy = {i : self.safeDivision(confusion_matrix[i][i], float(sum(confusion_matrix[i].values()))) for i in pattern_categories}
        
        # precision = number of patterns correctly classified as i / number of patterns classified as i
        precision = {i : self.safeDivision(confusion_matrix[i][i], float(sum([confusion_matrix[j][i] for j in pattern_categories]))) for i in pattern_categories}

        return accuracy, precision, self.timer, len(self.result.items())

    def safeDivision(self, a, b):
        return 0 if b == 0 else a/b