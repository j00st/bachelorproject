import csv
import json
import preprocessor

from helpers import showImage, readImage, writeImage, convertDict, extendDict, exportToCsv, exportToJson

class Harvester:

    def __init__(self, settings):
        self.settings = settings

    def harvest(self, input_path, output_path):
        results = {}

        with open(input_path, 'rb') as f:
            reader = csv.reader(f)
            for row in reader:
                results[row[0]] = json.loads(row[4])

        results = convertDict(results)

        for pattern_name, methods in results.items():
            image = readImage(self.settings['data_path']+pattern_name)

            for method, params in methods.items():
                image = preprocessor.do(image, method, params)

            writeImage(image, output_path+pattern_name)
