import collections
import csv
import cv2
import json

from matplotlib import pyplot as plt

# Convert unicode dict to string dict
def convertDict(data):
    if isinstance(data, basestring):
        return str(data)
    elif isinstance(data, collections.Mapping):
        return dict(map(convertDict, data.iteritems()))
    elif isinstance(data, collections.Iterable):
        return type(data)(map(convertDict, data))
    else:
        return data

# Extend dict d with dict u
def extendDict(d, u):
    for k, v in u.iteritems():
        if isinstance(v, collections.Mapping):
            r = extendDict(d.get(k, {}), v)
            d[k] = r
        else:
            d[k] = u[k]
    return d

def readCsv(filepath):
    return [row for row in csv.reader(open(filepath, 'rb'), delimiter=',', quotechar='"')]

# Export list to CSV
def exportToCsv(filename, rows):
    f = open(filename, 'a')
    wr = csv.writer(f, quoting=csv.QUOTE_MINIMAL, delimiter=',')

    for row in rows:
        wr.writerow(row)

    f.close()

# Export object to JSON
def exportToJson(filename, obj):
    json.dump(obj, open(filename, 'w+'))

# Read image with CV2
def readImage(path, read_type = cv2.IMREAD_COLOR):
    return cv2.imread(path, read_type)

def showImage(image, title = 'Image'):
    plt.imshow(image, cmap = 'gray')
    plt.title(title), plt.xticks([]), plt.yticks([])
    plt.show()

def writeImage(image, filename):
	cv2.imwrite(filename, image)