import argparse
import csv
import cv2
import data
import glob
import sys
import json
import os.path
import pprint
import collections
import random
import time

from helpers import showImage, readImage, convertDict, extendDict, exportToCsv, exportToJson

import preprocessor

CHAR_WIDTH = 100
CHAR_HEIGHT = 100
EXAMPLES_LOCATION = '../data/data_characters_50x50/'
OUTPUT_LOCATION = '../data/data_100x100_25/'

def overlayImage(char, full_image, row, col):
	h, w, d = char.shape
	x = col*CHAR_WIDTH+(CHAR_WIDTH-w)/2
	y = row*CHAR_HEIGHT+(CHAR_HEIGHT-h)/2

	for c in range(0,3):
		full_image[y:y+h, x:x+w, c] = char[:,:,c] * (char[:,:,3]/255.0) + full_image[y:y+h, x:x+w, c] * (1.0 - char[:,:,3]/255.0)

	return full_image

def extractImage(full_image, row, col):
	x = col*CHAR_WIDTH
	y = row*CHAR_WIDTH
	w, h = CHAR_WIDTH, CHAR_HEIGHT

	return full_image[y:y+h, x:x+h]

def main():
	characters = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']

	full_image = readImage('../data/data_bg.png', -1)
	
	num_cols = full_image.shape[1]/CHAR_WIDTH

	params = []

	for row in range(len(characters)):
		original_char = readImage(EXAMPLES_LOCATION+characters[row]+'.png', -1)
		params.append([])
		for col in range(num_cols):
			perspective = [\
				generateGaussParam(0, 5), generateGaussParam(0, 5),\
				generateGaussParam(100, 5), generateGaussParam(0, 5),\
				generateGaussParam(0, 5), generateGaussParam(100, 5),\
				generateGaussParam(100, 5), generateGaussParam(100, 5),\
			]

			rotate = [random.uniform(-60, 60)]
			transparency = [random.uniform(.5, 1)]
			invert = [random.random()]
			resize = [random.uniform(.66666667, 1), random.uniform(.66666667, 1)]

			char = preprocessor.do(original_char, 'perspective', perspective)
			char = preprocessor.do(char, 'rotate', rotate)
			char = preprocessor.do(char, 'transparency', transparency)
			char = preprocessor.do(char, 'invert', invert)
			char = preprocessor.do(char, 'resize', resize)
			full_image = overlayImage(char, full_image, row, col)

			params[row].append({ 'perspective' : perspective, 'resize' : resize, 'rotate' : rotate, 'transparency' : transparency, 'invert' : invert })

	showImage(full_image)
	cv2.imwrite('../generated_data.png', full_image)

	export_data = []

	for row in range(len(characters)):
		for col in range(num_cols):
			char = extractImage(full_image, row, col)
			number = str(col) if col > 9 else str(0)+str(col)
			cv2.imwrite(OUTPUT_LOCATION+characters[row]+number+'.jpg', char)
			export_data.append([characters[row]+number+'.jpg', json.dumps(params[row][col])])

	exportToCsv(OUTPUT_LOCATION+'params.csv', export_data)

def generateRandomFloatParam(min, max):
    return random.uniform(min, max)

def generateGaussParam(mean, sd, min_val=False, max_val=False):
    param = random.gauss(mean, sd)
    if min_val:
        param = max(param, min_val)
    if max_val:
        param = min(param, max_val)
    return param

if __name__ == '__main__':
    main()