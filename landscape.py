import argparse
import datetime
import data
import sys
import json
import os.path
import time
import numpy
import preprocessor
import random

from template_matcher import TemplateMatcher
from classifier import Classifier
from harvester import Harvester

from helpers import showImage, readImage, convertDict, extendDict, exportToCsv, exportToJson


image = readImage('../data/data_50x50_25/a21.jpg')
template = readImage('../data/templates_50x50_25/a.jpg')

matcher = TemplateMatcher()

#params = {"binarisation": [108.66439508747648], "rotate": [-47.51456401232571], "resize": [1/(0.7410354348402335/0.833333333) , 1/(0.6949414275345939/0.833333333)]}

params = {"binarisation": [random.uniform(0.0, 255.0)], "rotate": [random.uniform(-60.0, 60.0)], "resize": [random.uniform(0.7, 1.4), random.uniform(0.7, 1.4)]}

"""
print 'angle,match'
for i in [x/10.0 for x in range(0, 3600)]:
	new_image = preprocessor.do(image, 'rotate', [i])
	new_image = preprocessor.do(new_image, 'resize', params['resize'])
	new_image = preprocessor.do(new_image, 'binarisation', params['binarisation'])

	res = matcher.match(new_image, template, True)

	print str(i)+','+str(res)

"""
"""
image = preprocessor.do(image, 'rotate', params['rotate'])
image = preprocessor.do(image, 'resize', params['resize'])

print 'threshold,match'
for i in range(0, 255):
	new_image = preprocessor.do(image, 'binarisation', [i])
	res = matcher.match(new_image, template, True)

	print str(i)+','+str(res)

"""
image = preprocessor.do(image, 'rotate', params['rotate'])

print 'resize_x,match'
for i in [x/100.0 for x in range(80,150)]:
	new_image = preprocessor.do(image, 'resize', [i, params['resize'][1]])
	new_image = preprocessor.do(new_image, 'binarisation', params['binarisation'])
	res = matcher.match(new_image, template, True)

	print str(i)+','+str(res)

"""

image = preprocessor.do(image, 'rotate', params['rotate'])

print 'resize_y,match'
for i in [x/100.0 for x in range(80,150)]:
	new_image = preprocessor.do(image, 'resize', [params['resize'][0], i])
	new_image = preprocessor.do(new_image, 'binarisation', params['binarisation'])
	res = matcher.match(new_image, template, True)

	print str(i)+','+str(res)
"""