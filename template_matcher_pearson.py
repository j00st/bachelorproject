import numpy
import cv2
from matplotlib import pyplot as plt
import time

class TemplateMatcher:

    def __init__(self):
        pass

    def match(self, image, template, plot = False):
        template_height, template_width = template.shape[0:2]
        image_height, image_width = image.shape[0:2]

        #image = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)
        #template = cv2.cvtColor(template, cv2.COLOR_RGB2GRAY)

        mean_template = numpy.mean(template)
        std_template = numpy.std(template)

        result = numpy.abs([[self.pearson(image[row:row+template_height,col:col+template_width], template, mean_template, std_template) for col in range(0, image_width-template_width)] for row in range(0, image_height-template_height)])

        print numpy.max(result), numpy.min(result)

        if plot:
            y, x = numpy.where(result == numpy.max(result))[0:2]
            y, x = y[0], x[0]
            rectangle = (x, y), (x+template_width, y+template_height)
            self.plot(template, result, image, rectangle)

        return numpy.max(result)

    def pearson(self, image, template, mean_template, std_template):
        mean_image = numpy.mean(image)
        std_image = numpy.std(image)

        if std_image == 0 or std_template == 0:
            return 0

        return numpy.mean((image-mean_image)*(template-mean_template))/(std_image*std_template)

    def plot(self, template, res, image, rectangle):
        cv2.rectangle(image, rectangle[0], rectangle[1], 127, 1)
        
        plt.subplot(131),plt.imshow(template, cmap = 'gray')
        plt.title('Template'), plt.xticks([]), plt.yticks([])

        plt.subplot(132),plt.imshow(res, cmap = 'gray')
        plt.title('Matching Result'), plt.xticks([]), plt.yticks([])

        plt.subplot(133),plt.imshow(image, cmap = 'gray')
        plt.title('Detected Point'), plt.xticks([]), plt.yticks([])

        plt.show()