import glob
import re

# Get train data
# Returns list of paths to images
def getFiles(filepath):
    return glob.glob(filepath)

# Strip the folder names from a path, keep filename
def getFilename(path):
    return path.split('/')[-1]

# Get the actual category of a pattern from its filename
# Example "a05.jpg" => "a", "a.jpg" => "a"
def getCategory(pattern_name):
    return re.sub('\d', '', pattern_name.replace('.jpg', ''))