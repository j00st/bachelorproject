import cv2
import numpy as np

def do(image, method, params):
    try:
        new_image = eval(method+'(image, '+(', '.join(str(x) for x in params))+')')
    except NameError:
        print 'Method does not exist'
        return image
    else:
        return new_image

def binarisation(image, threshold):
    image = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)
    threshold = limitIntensity(threshold)
    #binary_map = [255.0 if x > threshold else 0.0 for x in range(0, 256)]
    #rows, cols  = range(0, len(image)), range(0, len(image[0]))

    image = cv2.threshold(image, threshold, 255, cv2.THRESH_BINARY)[1]

    #image = np.array([[binary_map[image[r][c]] for c in cols] for r in rows], np.uint8)

    return cv2.cvtColor(image, cv2.COLOR_GRAY2RGB)

def contrast(image, amount):
    amount = max(-1, amount)
    m = _getMeanIntensity(image)
    contrast_map = [limitIntensity(round(x+(x-m)*amount)) for x in range(0, 256)]
    rows, cols  = range(0, len(image)), range(0, len(image[0]))

    image = np.array([[[contrast_map[v] for v in image[r][c]] for c in cols] for r in rows], np.uint8)

    return image

def gamma(image, gamma):
    gamma_map = [limitIntensity(round(np.power(x, gamma))) for x in range(0, 256)]
    rows, cols  = range(0, len(image)), range(0, len(image[0]))

    image = np.array([[[gamma_map[v] for v in image[r][c]] for c in cols] for r in rows], np.uint8)

    return image

def hue(image, amount):
    image = cv2.cvtColor(image, cv2.COLOR_RGB2HLS)
    rows, cols  = range(0, len(image)), range(0, len(image[0]))

    for r in rows:
        for c in cols:
            image[r][c][0] = image[r][c][0]+amount

    return cv2.cvtColor(image, cv2.COLOR_HLS2RGB)

def invert(image, p):
    if p > .5:
        for c in range(0, 3):
            image[:,:,c] = 255.0 - image[:,:,c]

    return image

# corners: [x_left, y_top, x_right, y_top, x_left, y_bottom, x_right, y_bottom]
def perspective(image, c1_x, c1_y, c2_x, c2_y, c3_x, c3_y, c4_x, c4_y):
    #image = cv2.cvtColor(image, cv2.COLOR_BGR2BGRA)
    h,w = image.shape[0:2]
    boundaries = np.float32([[0, 0], [w, 0], [0, h], [w, h]])
    corners = np.float32([[c1_x, c1_y], [c2_x, c2_y], [c3_x, c3_y], [c4_x, c4_y]])
    M = cv2.getPerspectiveTransform(corners, boundaries)
    return cv2.warpPerspective(image, M, (w, h), borderMode=cv2.BORDER_REPLICATE)

def transparency(image, alpha):
    image[:,:,3] = alpha*image[:,:,3]
    return image

def resize(image, x_scale, y_scale = False):
    if y_scale == False: 
        y_scale = x_scale
    return cv2.resize(image, (0,0), fx=x_scale, fy=y_scale)

def rotate(image, angle):
    image_center = tuple(np.array(image.shape)/2)[0:2]
    rot_mat = cv2.getRotationMatrix2D(image_center, angle, 1.0)
    result = cv2.warpAffine(image, rot_mat, image.shape[0:2], flags=cv2.INTER_LINEAR, borderMode=cv2.BORDER_REFLECT)
    return result

##### HELPERS #####

def limitIntensity(v):
    return int(min(255, max(0, v)))

def _getMeanIntensity(image):
    m = 0
    for r in range(len(image)):
        for c in range(len(image[r])):
            for v in range(len(image[r][c])):
                m = m + image[r][c][v]/3
    m = m / (len(image) * len(image[0]))
    return m

def _getExpectedLocation(image):
    m = _getMeanIntensity(image)
    mx = 0
    my = 0
    n = 0

    for r in range(len(image)):
        for c in range(len(image[r])):
            mrgb = 0
            for v in range(len(image[r][c])):
                mrgb = mrgb + image[r][c][v]
            mrgb = mrgb / 3
            if mrgb < m:
                mx = mx + c
                my = my + r
                n = n + 1
    
    mx = mx/n
    my = my/n
    
    return mx, my