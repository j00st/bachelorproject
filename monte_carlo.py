import copy
import data
import random
import time
import numpy
import preprocessor
import sys

from helpers import showImage, readImage, convertDict, extendDict, exportToCsv, exportToJson

class MonteCarlo:

    def __init__(self, matcher, settings, image = False):
        self.matcher = matcher
        self.settings = settings
        self.image = image

    def run(self):
        N = max(self.settings['optimization']['n'], 1)
        if self.image is not False:
            patterns = ['input_image']
        else:
            patterns = data.getFiles(self.settings['data_path']+'*.jpg')
            
        templates = data.getFiles(self.settings['templates_path']+'*.jpg')
        template_images = {}
        result = {}

        for pattern in patterns:
            # Load the image
            if self.image is not False:
                pattern_image = self.image
                pattern_name = 'input_image'
                pattern_category = 'input_image'
            else:
                pattern_image = readImage(pattern)
                pattern_name = data.getFilename(pattern)
                pattern_category = data.getCategory(pattern_name)

            # Get preprocessing methods from self.settings
            methods = self.settings['preprocessing']['methods']

            print '\rClassifying '+pattern_name,
            sys.stdout.flush()

            best_match = 0
            winner = {'class' : self.settings['unknown_class'], 'category' : pattern_category, 'match' : 0, 'preprocessing' : ''}

            for i in range(0, N):
                # Reset morphed image
                morphed_image = copy.deepcopy(pattern_image)
                params = {}

                # Preprocessing, Monte Carlo style
                for method in methods:
                    # Remember preprocessing methods and params so they can be logged
                    params[method['name']] = self.generateMethodParams(method)
                    # Perform preprocessing
                    morphed_image = preprocessor.do(morphed_image, method['name'], params[method['name']])

                # Match with templates
                for template in templates:
                    template_name = data.getFilename(template)
                    template_category = data.getCategory(template_name)

                    # Cache template image
                    if template_name not in template_images:
                        template_images[template_name] = readImage(template)

                    # Get template image
                    template_image = template_images[template_name]

                    # Calculate correlation
                    match = self.matcher.match(morphed_image, template_image, self.settings['template_matching']['absval'])

                    if match > best_match:
                        best_match = match
                        winner = {'class' : template_category, 'category' : pattern_category, 'match' : match, 'preprocessing' : params}
                    
            result[pattern_name] = winner

        return result

    def generateMethodParams(self, method):
            params = []
            for param in method['params']:
                if param['type'] == 'random_float':
                    params.append(self.generateRandomFloatParam(param['min'], param['max']))
                elif param['type'] == 'gaussian':
                    min_val = param['min'] if 'min' in param else False
                    max_val = param['max'] if 'max' in param else False
                    params.append(self.generateGaussParam(param['mean'], param['sd'], min_val, max_val))
            return params

    def generateRandomFloatParam(self, min, max):
        return random.uniform(min, max)

    def generateGaussParam(self, mean, sd, min_val=False, max_val=False):
        param = random.gauss(mean, sd)
        if min_val:
            param = max(param, min_val)
        if max_val:
            param = min(param, max_val)
        return param