import argparse
import datetime
import data
import sys
import json
import os.path
import time
import numpy
import preprocessor

from template_matcher import TemplateMatcher
from classifier import Classifier
from harvester import Harvester

from helpers import showImage, readImage, convertDict, extendDict, exportToCsv, exportToJson

# Settings for global use
settings = {}

# Classify data and save results
def classify(verbose):
    # Load matcher
    matcher = TemplateMatcher()

    # Load classifier
    classifier = Classifier(matcher, settings, verbose)

    results = classifier.classify()
    accuracy, precision, timer, num_patterns = classifier.calcPerformance()

    print '\rClassification completed in '+str(round(timer, 3))+' seconds'
    print 'Accuracy = '+str(numpy.mean(accuracy.values()))+', precision = '+str(numpy.mean(precision.values()))+'\n'

    timestamp = round(time.time(), 3)
    date_time = datetime.datetime.fromtimestamp(int(timestamp)).strftime('%Y-%m-%d %H:%M:%S')

    filename = settings['config_name']+'_'+str(timestamp).replace('.', '')

    export_results = []
    for pattern_name, pattern_result in results.items():
        export_results.append([pattern_name, pattern_result['class'], pattern_result['category'], pattern_result['match'], json.dumps(pattern_result['preprocessing'])])

    exportToJson(settings['logs_path']+filename+'.json', settings)
    exportToCsv(settings['logs_path']+filename+'.csv', export_results)

    exportToCsv(settings['results_file'], [[date_time, settings['config_name'], str(timestamp).replace('.', ''), numpy.mean(accuracy.values()), numpy.mean(precision.values()), timer, num_patterns, settings['config_description']]])

# Test preprocessing methods
def testPreprocessor():
    pattern_name = raw_input('Pattern name: ')
    image = readImage(settings['data_path']+pattern_name)
    
    if image is not None:
        showImage(image, 'Original')

        while True:
            method = raw_input('Method: ')

            if method != '':
                method = method.split(' ')
                params = map(float, method[1:]) if len(method[1:]) > 0 else []
                method = method[0]
                timer = time.time()
                image = preprocessor.do(image, method, params)
                print 'Completed in '+str(time.time()-timer)+'s'
                showImage(image, 'Result')
            else:
                break
    else:
        print 'Image not found'

# Test template matcher
def testMatcher():
    # Load matcher
    matcher = TemplateMatcher()

    pattern_name = raw_input('Pattern name: ')
    template_name = raw_input('Template: ')

    pattern = readImage(settings['data_path']+pattern_name)
    template = readImage(settings['templates_path']+template_name+'.jpg')

    if pattern is None:
        print 'Pattern not found'
    elif template is None:
        print 'Template not found'
    else:
        max_val = matcher.match(pattern, template, settings['template_matching']['absval'], True)
        print 'Max: '+str(max_val)


def harvest(input_path, output_path):
    harvester = Harvester(settings)

    harvester.harvest(input_path, output_path)

def loadSettings(args):
    # Open config files
    default_config = open('config/default.json')
    user_config = open('config/'+args['config']) if args['config'] and os.path.isfile('config/'+args['config']) else False

    # Create settings dictionary
    settings = json.load(default_config)
    user_settings = json.load(user_config) if user_config else {}

    # Extend default settings with user-defined settings
    settings = convertDict(extendDict(settings, user_settings))

    return settings

# Main
def main():
    global settings

    # Parse command line arguments
    parser = argparse.ArgumentParser(description='This program takes the following arguments:')
    parser.add_argument('action', metavar='action', type=str, choices=['classify', 'preprocess', 'match', 'harvest'], help='action to perform (classifier, preprocessor, matcher)')
    parser.add_argument('-c', '--config', metavar='config', type=str, default=False, help='filename of json config file')
    parser.add_argument('-v', '--verbose', action='count', default=0, help='verbose mode')
    parser.add_argument('-hi', '--h_input', metavar='harvester_input', type=str, default=False, help='filepath of csv file')
    parser.add_argument('-ho', '--h_output', metavar='harvester_output', type=str, default=False, help='output location')
    args = vars(parser.parse_args())

    settings = loadSettings(args)

    print '\nConfig: '+settings['config_name']
    print 'Description: '+settings['config_description']
    print 'Dataset: '+str(len(data.getFiles(settings['data_path']+'*.jpg')))+' patterns\n'

    # Do action
    if args['action'] == 'classify':
        classify(args['verbose'])
    elif args['action'] == 'preprocess':
        testPreprocessor()
    elif args['action'] == 'match':
        testMatcher()
    elif args['action'] == 'harvest':
        harvest(args['h_input'], args['h_output'])


if __name__ == '__main__':
    main()