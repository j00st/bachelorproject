from scipy.stats.stats import pearsonr
import random

a = [random.random() for x in range(1, 1000)]
b = [random.random() for x in range(1, 1000)]

print pearsonr(a, b)[0]