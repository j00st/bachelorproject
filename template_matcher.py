import cv2
from matplotlib import pyplot as plt

class TemplateMatcher:

    def __init__(self):
        pass

    # Do the matching
    def match(self, image, template, absval, plot = False):
        h, w = template.shape[0:2]

        res = cv2.matchTemplate(image, template, cv2.TM_CCOEFF_NORMED)
        min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(res)

        if absval and abs(min_val) > abs(max_val):
            max_val = abs(min_val)
            max_loc = min_loc

        if plot:
            rectangle = max_loc, (max_loc[0]+w, max_loc[1]+h)
            self.plot(template, res, image, rectangle)

        return max_val

    # Plot matching result
    def plot(self, template, res, image, rectangle):
        cv2.rectangle(image, rectangle[0], rectangle[1], 127, 1)
        
        plt.subplot(131),plt.imshow(template, cmap = 'gray')
        plt.title('Template'), plt.xticks([]), plt.yticks([])

        plt.subplot(132),plt.imshow(res, cmap = 'gray')
        plt.title('Matching Result'), plt.xticks([]), plt.yticks([])

        plt.subplot(133),plt.imshow(image, cmap = 'gray')
        plt.title('Detected Point'), plt.xticks([]), plt.yticks([])

        plt.show()