import argparse
import cv2
import datetime
import data
import sys
import json
import os.path
import time
import numpy as np
import preprocessor

from template_matcher import TemplateMatcher
from classifier import Classifier
from harvester import Harvester

from helpers import showImage, readImage, convertDict, extendDict, exportToCsv, exportToJson

settings = {}

def classify(image):
    matcher = TemplateMatcher()
    classifier = Classifier(matcher, settings, 0)

    image = cv2.resize(image, (0,0), fx=0.25, fy=0.25)

    results = classifier.classify(image)['input_image']
    print results

    image = preprocessor.do(image, 'rotate', results['preprocessing']['rotate'])
    image = preprocessor.do(image, 'resize', results['preprocessing']['resize'])
    image = preprocessor.do(image, 'binarisation', results['preprocessing']['binarisation'])

    return results['class'], results['match'], image

def loadSettings(args):
    # Open config files
    default_config = open('config/default.json')
    user_config = open('config/'+args['config']) if args['config'] and os.path.isfile('config/'+args['config']) else False

    # Create settings dictionary
    settings = json.load(default_config)
    user_settings = json.load(user_config) if user_config else {}

    # Extend default settings with user-defined settings
    settings = convertDict(extendDict(settings, user_settings))

    return settings

# Main
def main():
    global settings

    # Parse command line arguments
    parser = argparse.ArgumentParser(description='This program takes the following arguments:')
    parser.add_argument('-c', '--config', metavar='config', type=str, default=False, help='filename of json config file')
    args = vars(parser.parse_args())

    settings = loadSettings(args)

    print settings

    classification, match, image = 'unk', 0, False

    c = cv2.VideoCapture(0)

    while(1):
        _, f = c.read()
        h, w, d = f.shape
        x, y = (w-200)/2, (h-200)/2

        cv2.rectangle(f, (x, y), (x+200, y+200), (0, 0, 255), 1)
        cv2.rectangle(f, (x+40, y+40), (x+160, y+160), (0, 255, 0), 1)

        f[y+201:y+250,x:x+201] = [0, 0, 255]

        cv2.putText(f, classification.upper()+' ('+str(round(match, 2))+')', (x+60, y+228), cv2.FONT_HERSHEY_DUPLEX, 0.5, (255,255,255))

        if image is not False:
            i_h, i_w, i_d = image.shape
            f[y:y+i_h, x+201:x+201+i_w] = image

        cv2.imshow('e2', f)

        key = cv2.waitKey(5)

        if key == 32:
            classification, match, image = classify(f[(h-200)/2:(h-200)/2+200, (w-200)/2:(w-200)/2+200])
        elif key == 27:
            break

    cv2.destroyAllWindows()

if __name__ == '__main__':
    main()