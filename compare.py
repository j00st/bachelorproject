import argparse
import datetime
import data
import glob
import sys
import json
import os.path
import time
import numpy
import preprocessor

from template_matcher import TemplateMatcher
from classifier import Classifier
from harvester import Harvester

from helpers import readCsv, convertDict

def readResults(filepath):
	results_data = readCsv(filepath)
	results = {}

	for row in results_data:
		results[row[0]] = json.loads(row[4])

	return convertDict(results)

params_data = readCsv('../data/data_25_params.csv')
params = {}

for row in params_data:
	params[row[0]] = json.loads(row[1])

params = convertDict(params)

results_files = ['140360541013','140360541013','140360541338','14036054145','140360543576','140360543781','14036054398','140360544173','140360571534','140360571853','140360572016','140360572089','140360595106','14036059533','140360595448','140360595556','14036062779','140360627792','140360627873','140360628085','140360703951','140360704067','140360704114','140360704505','140360769683','140360770127','140360770137','14036077015','140360844529','140360844917','140360845321','140360845472','140360916975','140360917054','140360917239','140360917367','140360980577','14036098064','140360980719','14036098122','140361276537','140361276956','140361277024','14036127788','140361693723','140361693762','140361694103','140361694533','140363124723','140363124743','140363126143','140363126602','140363292225','140363292589','140363292628','140363292744','140363488429','140363488825','140363489075','140363489522','140363807389','14036380741','140363807467','140363807949']
print 'config,resize_x,resize_y,rotate'

for results_file in results_files:
	results = readResults('../results/logs/extrafinal_'+results_file+'.csv')

	resize = [0, 0]
	rotate = 0


	data_len = len(params)

	for pattern_name in params:
		if results[pattern_name] != "":
			resize[0] += abs((5.0/6.0)/params[pattern_name]['resize'][0]-results[pattern_name]['resize'][0])
			resize[1] += abs((5.0/6.0)/params[pattern_name]['resize'][1]-results[pattern_name]['resize'][1])
			rotate += abs(params[pattern_name]['rotate'][0]+results[pattern_name]['rotate'][0])
		else:
			data_len -= 1

	resize[0] /= data_len
	resize[1] /= data_len
	rotate /= data_len

	print results_file+','+str(resize[0])+','+str(resize[1])+','+str(rotate)
